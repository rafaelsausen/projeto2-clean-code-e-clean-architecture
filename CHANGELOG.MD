# Change Log

All notable changes to this project will be documented in this file (if I remember to keep it updated kkkk).

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - 2023-02-09

Here we write upgrading notes for brands. I'll try to make them as
straightforward as possible.

### Added

- Included initial project to the clean code course.
- Configured eslint, prettier and editor config.
- Added jest to make tests.
- A initial project with a file and a test working fine.
- First class implemented with some changes that I made on my way.

### Changed

- nothing.

### Fixed

- nothing.

## [Unreleased] - 2023-02-13

Here we write upgrading notes for brands. I'll try to make them as
straightforward as possible.

### Added

- Aula 2.
- Corrigindo o Mailer que foi implementado sem o contrato.

### Changed

- nothing.

### Fixed

- nothing.

## [Unreleased] - 2023-02-15

Here we write upgrading notes for brands. I'll try to make them as
straightforward as possible.

### Added

- Aula 3.
- Adicionando order e item no banco.
- Organizando diretórios.
- Finalizando conexões do banco nos testes para os testes que o Branas esqueceu.
- Tipando todas as variáveis e corrigindo os testes onde precisava dos tipos.

### Changed

- nothing.

### Fixed

- nothing.


## [Unreleased] - 2023-02-16

Here we write upgrading notes for brands. I'll try to make them as
straightforward as possible.

### Added

Backend:
- Adicionei os outros pontos de entrada como foi mostrado na aula.

Frontend:

- Implementei o frontend.

### Changed

Backend:
- Tipei os objetos de alguns testes que eu não tinha percebido que poderiam ser tipados até o Branas mostrar na aula.
- Arrumei o CORS do HAPI que o Branas não fez na aula.
- Fiz os testes fechando as conexões para não dar os erros de conexão que apareciam nos testes do Branas.

## [Unreleased] - 2023-02-18

### Added

Backend:
- Separando os bounded contexts (catalog, checkout e freight).
- Todos usando seus próprios .env
- Trocando Express e Hapi pelo .env

## [Unreleased] - 2023-02-18

### Added

Backend:
- Bounded context stock adicionado com opção de porta e package pelo env

### Changed

Backend:
- Cobertura de testes para todos os módulos revisada e ampliada.
- Renomeando Data para Repositories. (Branas falou mas fez apenas no repositório novo).
