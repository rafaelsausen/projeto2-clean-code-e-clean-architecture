# Clean Code Course

Project developed during the clean code course.

## Description

I'll improve this project after each class to register the evolution.

## Errors and solutions

- While running migrations.
  Error: Error during Data Source initialization Error: Cannot find module...
  Solution: Comment the following line on file src/shared/infra/typeorm/index.ts

```
entities: paths,
```

Run migrations.
Uncomment the line.

- While running the application.
  Error: No metadata for "Table" was found.
  Solution: Uncomment the following line on file src/shared/infra/typeorm/index.ts

```
entities: paths,
```

## Executar


### Backend

#### Catalog

- Rodar o backend:
```
cd backend/catalog
yarn dev
```

- Rodar os testes:
```
cd backend/catalog
yarn jest --watchAll
```

#### Checkout

- Rodar o backend:
```
cd backend/checkout
yarn dev
```

- Rodar os testes:
```
cd backend/checkout
yarn jest --watchAll
```

#### Freight

- Rodar o backend:
```
cd backend/freight
yarn dev
```

- Rodar os testes:
```
cd backend/freight
yarn jest --watchAll
```

#### Stock

**Precisa subir o container do RabbitMQAdapter:

```
docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 -d rabbitmq:3.11-management
```

- Rodar o backend:
```
cd backend/stock
yarn dev
```

- Rodar os testes:
```
cd backend/stock
yarn jest --watchAll
```

### Frontend

- Rodar o backend:
```
cd frontend
yarn dev
```

- Rodar os testes:
```
cd frontend
yarn jest --watchAll
```

## Usar RabbitMQAdapter

Para usar o RabbitMQAdapter, suba o container com o comando:

```
docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 -d rabbitmq:3.11-management
```

Para acessar o gerenciador, no navegador insira o endereço: http://localhost:15672/#/, e utilize as credenciais: guest, guest.

No diretório do bounded context checkout execute o comando para produzir itens na fila: (cada execução do comando adiciona um item na fila e é necessário finalizar o comando com ctrl + c)

```
yarn tsnd src/queue_producer.ts
```

Para consumir, execute o comando: (também precisa do ctrl + c para finalizar)
```
yarn tsnd src/main_queue.ts
```
