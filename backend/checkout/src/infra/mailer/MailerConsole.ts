import Mailer from './Mailer';

export default class MailerConsole implements Mailer {
  async send(to: string, subject: string, message: string) {
    // eslint-disable-next-line no-console
    console.log(to, subject, message);
  }
}
