import Checkout from '../../application/Checkout';
import Queue from './interfaces/Queue';

export default class QueueController {
  constructor(readonly queue: Queue, readonly checkout: Checkout) {
    queue.on('orderPlaced', async function (input: any) {
      await checkout.execute(input);
    });
  }
}
