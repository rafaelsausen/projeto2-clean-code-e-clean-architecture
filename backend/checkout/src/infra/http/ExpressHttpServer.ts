/* eslint-disable @typescript-eslint/no-explicit-any */
import IHttpServer from './repositories/IHttpServer';
import express from 'express';
import cors from 'cors';

export default class ExpressHttpServer implements IHttpServer {
  app: any;

  constructor() {
    this.app = express();
    this.app.use(express.json());
    this.app.use(cors());
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  on(method: string, url: string, callback: Function): void {
    this.app[method](url, async function (req: any, res: any) {
      try {
        const output = await callback(req.params, req.body);
        res.json(output);
      } catch (error: any) {
        res.status(422).json({
          message: error.message,
        });
      }
    });
  }

  listen(port: number): void {
    // eslint-disable-next-line no-console
    console.log(
      `🏆 Server started with EXPRESS on port ${`${process.env.API_URL}/`}`,
    );
    return this.app.listen(port);
  }
}
