import Checkout from '../../application/Checkout';
import IHttpServer from '../http/repositories/IHttpServer';
import Queue from '../queue/interfaces/Queue';

export default class RestController {
  constructor(
    readonly httpServer: IHttpServer,
    readonly checkout: Checkout,
    readonly queue: Queue,
  ) {
    httpServer.on('get', '/products', async function (params: any, body: any) {
      const output = [{ idProduct: 4, description: 'D', price: 1000 }];
      return output;
    });

    httpServer.on(
      'post',
      '/checkout',
      async function (params: unknown, body: Input) {
        const output = await checkout.execute(body);
        return output;
        //// O código abaixo seria para receber a ordem e colocar na fila para processamento posterior
        // await queue.publish('placeOrder', body);
      },
    );
  }
}

type Input = {
  cpf: string;
  email?: string;
  items: { idProduct: number; quantity: number }[];
  coupon?: string;
};
