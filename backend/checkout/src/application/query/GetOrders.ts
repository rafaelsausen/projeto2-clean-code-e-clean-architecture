import IConnection from '../../infra/database/IConnection';

export default class GetOrders {
  constructor(readonly connection: IConnection) {}

  async execute() {
    const ordersData = await this.connection.query(
      'SELECT * FROM cccat9.order',
      [],
    );
    for (const orderData of ordersData) {
      orderData.items = await this.connection.query(
        'SELECT * FROM cccat9.item AS i JOIN cccat9.product AS p USING (id_product) WHERE id_order = $1',
        [orderData.id_order],
      );
    }
    return ordersData;
  }
}
