import dotenv from 'dotenv';
import Checkout from './application/Checkout';
import RestController from './infra/controller/RestController';
import CouponDataDatabase from './infra/repositories/CouponDataDatabase';
import OrderDataDatabase from './infra/repositories/OrderDataDatabase';
import PgPromiseConnection from './infra/database/PgPromiseConnection';
import CatalogGatewayHttp from './infra/gateway/CatalogGatewayHttp';
import FreightGatewayHttp from './infra/gateway/FreightGatewayHttp';
// import StockGatewayHttp from './infra/gateway/StockGatewayHttp';
// import QueueController from './infra/queue/QueueController';
import RabbitMQAdapter from './infra/queue/RabbitMQAdapter';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ExpressHttpServer from './infra/http/ExpressHttpServer';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import HapiHttpServer from './infra/http/HapiHttpServer';

dotenv.config();
export default async function main() {
  const connection = new PgPromiseConnection();
  const httpServer =
    Number(process.env.API_PACKAGE) === 1
      ? new HapiHttpServer()
      : new ExpressHttpServer();
  const couponData = new CouponDataDatabase(connection);
  const orderData = new OrderDataDatabase(connection);
  const freightGateway = new FreightGatewayHttp();
  const catalogGateway = new CatalogGatewayHttp();
  // const stockGateway = new StockGatewayHttp();
  const queue = new RabbitMQAdapter();
  await queue.connect();
  const checkout = new Checkout(
    catalogGateway,
    couponData,
    orderData,
    freightGateway,
    // stockGateway,
    queue,
  );
  new RestController(httpServer, checkout, queue);
  // O código abaixo era para o exemplo final, onde o pedido seria enviado (mesmo com o catalog fora)
  // postando placeOrder (ao invés de orderPlaced) no RabbitMQueue e posteriormente seria processado.
  // Fiz o exemplo, arrumei o teste e depois desfiz porque todos os outros testes teriam que ser arrumados.
  // new QueueController(queue, checkout);
  httpServer.listen(Number(process.env.API_PORT));
}

main();
