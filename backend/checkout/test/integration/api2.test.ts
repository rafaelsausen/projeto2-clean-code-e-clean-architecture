import axios from 'axios';

axios.defaults.validateStatus = function () {
  return true;
};
import dotenv from 'dotenv';
import supertest from 'supertest';
import request from 'supertest';

import main from '../../src/main_api';

supertest(main);

const wrongCpf = '987.654.321-01';
const rightCpf = '987.654.321-00';
const threeItems = [
  { idProduct: 1, quantity: 1 },
  { idProduct: 2, quantity: 1 },
  { idProduct: 3, quantity: 3 },
];

jest.setTimeout(15000);

dotenv.config();
const url = `${process.env.API_URL}`;

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

beforeAll(async () => {
  await delay(1000);
});

test('Não deve fazer um pedido com cpf inválido', async function () {
  const input = {
    cpf: wrongCpf,
  };
  const res = await request(url).post('/checkout').send(input);
  expect(res.body.message).toBe('Invalid cpf');
});

test('Deve fazer um pedido com 3 produtos', async function () {
  const input = {
    cpf: rightCpf,
    items: threeItems,
  };
  const response = await request(url).post('/checkout').send(input);
  const output = response.body;
  expect(output.total).toBe(6370);
});

// Esse teste será feito na próxima aula.
test.skip('Não deve fazer pedido com produto que não existe', async function () {
  const input = {
    cpf: rightCpf,
    items: [{ idProduct: 5, quantity: 1 }],
  };
  const response = await request(url).post('/checkout').send(input);
  expect(response.status).toBe(422);
  const output = response.body;
  expect(output.message).toBe('Product not found');
});

test('Deve fazer um pedido com 3 produtos com cupom de desconto', async function () {
  const input = {
    cpf: rightCpf,
    items: threeItems,
    coupon: 'VALE20',
  };
  const response = await request(url).post('/checkout').send(input);
  const output = response.body;
  expect(output.total).toBe(5152);
});

test('Deve fazer um pedido com 3 produtos com cupom de desconto expirado', async function () {
  const input = {
    cpf: rightCpf,
    items: threeItems,
    coupon: 'VALE20_EXPIRED',
  };
  const response = await request(url).post('/checkout').send(input);
  const output = response.body;
  expect(output.total).toBe(6370);
});

test('Deve fazer um pedido com quantidade negativa', async function () {
  const input = {
    cpf: rightCpf,
    items: [{ idProduct: 1, quantity: -1 }],
    coupon: 'VALE20',
  };
  const response = await request(url).post('/checkout').send(input);
  expect(response.status).toBe(422);
  const output = response.body;
  expect(output.message).toBe('Quantity must be positive');
});

test('Não pode duplicar produto', async function () {
  const input = {
    cpf: rightCpf,
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 1, quantity: 1 },
    ],
  };
  const response = await request(url).post('/checkout').send(input);
  expect(response.status).toBe(422);
  const output = response.body;
  expect(output.message).toBe('Cannot duplicate product');
});

test('Deve fazer um pedido e calcular o frete', async function () {
  const input = {
    cpf: rightCpf,
    items: [{ idProduct: 1, quantity: 1 }],
  };
  const response = await request(url).post('/checkout').send(input);
  const output = response.body;
  expect(output.total).toBe(1030);
});

test('Deve fazer um pedido calculando o frete mínimo', async function () {
  const input = {
    cpf: rightCpf,
    items: [{ idProduct: 3, quantity: 1 }],
  };
  const response = await request(url).post('/checkout').send(input);
  const output = response.body;
  expect(output.total).toBe(40);
});

test('Teste do endpoint products', async function () {
  const response = await request(url).get('/products');
  expect(response.status).toBe(200);
  const output = response.body;
  expect(output[0].idProduct).toBeGreaterThanOrEqual(1);
});
