import Checkout from '../../src/application/Checkout';
import CouponData from '../../src/domain/repositories/CouponData';
import ProductData from '../../src/domain/repositories/ProductData';
import sinon from 'sinon';
import CurrencyGateway from '../../src/infra/gateway/CurrencyGatewayRandom';
import OrderData from '../../src/domain/repositories/OrderData';
import Currencies from '../../src/domain/entities/Currencies';
import Product from '../../src/domain/entities/Product';
import Coupon from '../../src/domain/entities/Coupon';
import Cpf from '../../src/domain/entities/Cpf';
import FreightGatewayHttp from '../../src/infra/gateway/FreightGatewayHttp';
import CatalogGatewayHttp from '../../src/infra/gateway/CatalogGatewayHttp';
import MailerConsole from '../../src/infra/mailer/MailerConsole';
// import StockGatewayHttp from '../../src/infra/gateway/StockGatewayHttp';

let checkout: Checkout;

const productData: ProductData = {
  async getProduct(idProduct: number): Promise<Product> {
    const products: { [idProduct: number]: Product } = {
      1: new Product(1, 'A', 1000, 100, 30, 10, 3, 'BRL'),
      2: new Product(2, 'B', 5000, 50, 50, 50, 22, 'BRL'),
      3: new Product(3, 'C', 30, 10, 10, 10, 0.9, 'BRL'),
      4: new Product(4, 'D', 100, 100, 30, 10, 3, 'USD'),
    };
    return products[idProduct];
  },
};

const couponData: CouponData = {
  async getCoupon(code: string): Promise<Coupon> {
    const coupons: { [idProduct: string]: Coupon } = {
      VALE20: new Coupon('VALE20', 20, new Date('2022-12-01T10:00:00')),
      VALE20_EXPIRED: new Coupon(
        'VALE20_EXPIRED',
        20,
        new Date('2022-10-01T10:00:00'),
      ),
    };
    return coupons[code];
  },
};

const orderData: OrderData = {
  async save(): Promise<void> {
    return;
  },
  async getByCpf(): Promise<Cpf> {
    return new Cpf('987.654.321-00');
  },
  async count(): Promise<number> {
    return 1;
  },
  async clean(): Promise<void> {
    return;
  },
};

const freightGateway = new FreightGatewayHttp();
const catalogGateway = new CatalogGatewayHttp();
// const stockGateway = new StockGatewayHttp();

beforeEach(function () {
  checkout = new Checkout(
    catalogGateway,
    couponData,
    orderData,
    freightGateway,
    // stockGateway,
  );
});

test('Deve fazer um pedido com 3 produtos', async function () {
  const input = {
    cpf: '987.654.321-00',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
    ],
  };
  const output = await checkout.execute(input);
  expect(output.total).toBe(6370);
});

test('Deve fazer um pedido com 4 produtos com moedas diferentes', async function () {
  const currencies = new Currencies();
  currencies.addCurrency('USD', 2);
  currencies.addCurrency('BRL', 1);
  const currencyGatewayStub = sinon
    .stub(CurrencyGateway.prototype, 'getCurrencies')
    .resolves(currencies);
  const mailerSpy = sinon.spy(MailerConsole.prototype, 'send');
  const input = {
    cpf: '987.654.321-00',
    email: 'teste@teste.com',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
      { idProduct: 4, quantity: 1 },
    ],
  };
  const output = await checkout.execute(input);
  expect(output.total).toBe(6600);
  currencyGatewayStub.restore();
  mailerSpy.restore();
});

test('Deve fazer um pedido com 4 produtos com moedas diferentes com mock', async function () {
  const currencies = new Currencies();
  currencies.addCurrency('USD', 2);
  currencies.addCurrency('BRL', 1);
  const currencyGatewayMock = sinon.mock(CurrencyGateway.prototype);
  currencyGatewayMock.expects('getCurrencies').once().resolves(currencies);
  const input = {
    cpf: '987.654.321-00',
    email: 'teste@teste.com',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
      { idProduct: 4, quantity: 1 },
    ],
  };
  const output = await checkout.execute(input);
  expect(output.total).toBe(6600);
  currencyGatewayMock.verify();
  currencyGatewayMock.restore();
});

test('Deve fazer um pedido com 4 produtos com moedas diferentes com fake', async function () {
  const input = {
    cpf: '987.654.321-00',
    email: 'teste@teste.com',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
      { idProduct: 4, quantity: 1 },
    ],
  };
  const currencies = new Currencies();
  currencies.addCurrency('USD', 2);
  currencies.addCurrency('BRL', 1);
  const checkout = new Checkout(
    productData,
    couponData,
    orderData,
    freightGateway,
    // stockGateway,
  );
  const output = await checkout.execute(input);
  expect(output.total).toBe(6700);
});
test('Deve fazer um pedido com 3 produtos com código do pedido', async function () {
  const input = {
    cpf: '987.654.321-00',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
    ],
  };
  const output = await checkout.execute(input);
  expect(output.code).toBe('202300000001');
});

test('Deve fazer um pedido com 3 produtos com cupom de desconto', async function () {
  const input = {
    cpf: '987.654.321-00',
    coupon: 'VALE20',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
    ],
  };
  const output = await checkout.execute(input);
  expect(output.code).toBe('202300000001');
});

test('Deve fazer um pedido com 3 produtos com CEP de origem e destino', async function () {
  const input = {
    from: '22030060',
    to: '88015600',
    cpf: '987.654.321-00',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
    ],
  };
  const output = await checkout.execute(input);
  expect(output.total).toBe(6307.06);
});
