import Checkout from '../../src/application/Checkout';
import CouponDataDatabase from '../../src/infra/repositories/CouponDataDatabase';
import OrderDataDatabase from '../../src/infra/repositories/OrderDataDatabase';
import PgPromiseConnection from '../../src/infra/database/PgPromiseConnection';
import FreightGatewayHttp from '../../src/infra/gateway/FreightGatewayHttp';
import CatalogGatewayHttp from '../../src/infra/gateway/CatalogGatewayHttp';
import GetOrders from '../../src/application/query/GetOrders';

test('Deve consultar os pedidos', async function () {
  const connection = new PgPromiseConnection();
  const couponData = new CouponDataDatabase(connection);
  const orderData = new OrderDataDatabase(connection);
  const freightGateway = new FreightGatewayHttp();
  const catalogGateway = new CatalogGatewayHttp();
  const checkout = new Checkout(
    catalogGateway,
    couponData,
    orderData,
    freightGateway,
  );
  const input = {
    cpf: '987.654.321-00',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
    ],
  };
  await checkout.execute(input);
  const getOrderByCpf = new GetOrders(connection);
  const output = await getOrderByCpf.execute();
  expect(output[1].id_order).toBeGreaterThanOrEqual(1);
  await connection.close();
});
