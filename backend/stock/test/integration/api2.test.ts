import axios from 'axios';

axios.defaults.validateStatus = function () {
  return true;
};
import dotenv from 'dotenv';
import supertest from 'supertest';
import request from 'supertest';

import main from '../../src/main_api';

supertest(main);

jest.setTimeout(15000);

dotenv.config();
const url = `${process.env.API_URL}`;

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

beforeAll(async () => {
  await delay(1000);
});

test('Deve testar a api de estoque', async function () {
  await request(url).post('/cleanStock');
  const response1 = await request(url).post('/calculateStock').send({
    idProduct: 1,
  });
  const output1 = response1.body;
  expect(output1.total).toBe(0);
  const input1 = {
    items: [{ idProduct: 1, quantity: 10 }],
  };
  await request(url).post('/increaseStock').send(input1);
  const response2 = await request(url).post('/calculateStock').send({
    idProduct: 1,
  });
  const output2 = response2.body;
  expect(output2.total).toBe(10);
  const input2 = {
    items: [{ idProduct: 1, quantity: 2 }],
  };
  await request(url).post('/decreaseStock').send(input2);
  const response3 = await request(url).post('/calculateStock').send({
    idProduct: 1,
  });
  const output3 = response3.body;
  expect(output3.total).toBe(8);
});

test('Deve retornar erro', async function () {
  await request(url).post('/cleanStock');
  const response1 = await request(url).post('/calculateStock').send({
    idProduct: 0,
  });
  expect(response1.status).toBe(422);
});
