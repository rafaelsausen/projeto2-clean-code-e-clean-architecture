import PgPromiseConnection from '../../src/infra/database/PgPromiseConnection';
import QueueController from '../../src/infra/queue/QueueController';
import QueueMemory from '../../src/infra/queue/QueueMemory';
import DecreaseStock from '../../src/application/DecreaseStock';
import StockEntryRepositoryDatabase from '../../src/infra/repository/StockEntryRepositoryDatabase';
import IQueue from '../../src/infra/queue/IQueue';

test('Deve testar com a fila', async function () {
  const queue: IQueue = new QueueMemory();
  const connection = new PgPromiseConnection();
  const stockEntryRepository = new StockEntryRepositoryDatabase(connection);
  const decreaseStock = new DecreaseStock(stockEntryRepository);
  new QueueController(queue, decreaseStock);
  const input = {
    cpf: '987.654.321-00',
    items: [
      { idProduct: 1, quantity: 1 },
      { idProduct: 2, quantity: 1 },
      { idProduct: 3, quantity: 3 },
    ],
  };
  await queue.publish('checkout', input);
  await queue.publish('orderPlaced', input);
  await stockEntryRepository.clean();
  await connection.close();
});
