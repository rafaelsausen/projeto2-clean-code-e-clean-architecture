import DecreaseStock from '../../application/DecreaseStock';
import IQueue from './IQueue';

export default class QueueController {
  constructor(readonly queue: IQueue, readonly decreaseStock: DecreaseStock) {
    queue.on('orderPlaced', async function (input: any) {
      await decreaseStock.execute(input);
    });
  }
}
