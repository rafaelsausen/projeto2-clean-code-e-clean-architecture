import StockEntry from '../../domain/entities/StockEntry';
import StockEntryRepository from '../../domain/repositories/StockEntryRepository';
import IConnection from '../database/IConnection';

export default class StockEntryRepositoryDatabase
  implements StockEntryRepository
{
  constructor(readonly connection: IConnection) {}

  async save(stockEntry: StockEntry): Promise<void> {
    await this.connection.query(
      'INSERT INTO cccat9.stock_entry (id_product, operation, quantity) VALUES ($1, $2, $3)',
      [stockEntry.idProduct, stockEntry.operation, stockEntry.quantity],
    );
  }

  async getByIdProduct(idProduct: number): Promise<StockEntry[]> {
    const stockEntriesData = await this.connection.query(
      'SELECT * FROM cccat9.stock_entry WHERE id_product = $1',
      [idProduct],
    );
    const stockEntries: StockEntry[] = [];
    for (const stockEntryData of stockEntriesData) {
      stockEntries.push(
        new StockEntry(
          stockEntryData.id_product,
          stockEntryData.operation,
          stockEntryData.quantity,
        ),
      );
    }
    return stockEntries;
  }

  async clean(): Promise<void> {
    await this.connection.query('DELETE FROM cccat9.stock_entry', []);
  }
}
