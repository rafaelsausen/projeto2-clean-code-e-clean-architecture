import StockCalculator from '../domain/entities/StockCalculator';
import StockEntryRepository from '../domain/repositories/StockEntryRepository';

export default class CalculateStock {
  constructor(readonly stockEntryRepository: StockEntryRepository) {}

  async execute(idProduct: number): Promise<Output> {
    if (idProduct <= 0) {
      throw new Error('Invalid Product');
    }
    const stockEntries = await this.stockEntryRepository.getByIdProduct(
      idProduct,
    );
    const total = StockCalculator.calculate(stockEntries);
    return {
      total,
    };
  }
}

type Output = {
  total: number;
};
