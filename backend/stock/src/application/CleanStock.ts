import StockEntryRepository from '../domain/repositories/StockEntryRepository';

export default class CleanStock {
  constructor(readonly stockEntryRepository: StockEntryRepository) {}

  async execute(): Promise<void> {
    await this.stockEntryRepository.clean();
  }
}
