import dotenv from 'dotenv';
import CalculateStock from './application/CalculateStock';
import CleanStock from './application/CleanStock';
import DecreaseStock from './application/DecreaseStock';
import IncreaseStock from './application/IncreaseStock';
import RestController from './infra/controller/RestController';
import PgPromiseConnection from './infra/database/PgPromiseConnection';
import ExpressHttpServer from './infra/http/ExpressHttpServer';
import HapiHttpServer from './infra/http/HapiHttpServer';
import QueueController from './infra/queue/QueueController';
import RabbitMQAdapter from './infra/queue/RabbitMQAdapter';
import StockEntryRepositoryDatabase from './infra/repository/StockEntryRepositoryDatabase';

dotenv.config();

export default async function main() {
  const connection = new PgPromiseConnection();
  const httpServer =
    Number(process.env.API_PACKAGE) === 1
      ? new HapiHttpServer()
      : new ExpressHttpServer();
  const stockEntryRepository = new StockEntryRepositoryDatabase(connection);
  const calculateStock = new CalculateStock(stockEntryRepository);
  const increaseStock = new IncreaseStock(stockEntryRepository);
  const decreaseStock = new DecreaseStock(stockEntryRepository);
  const cleanStock = new CleanStock(stockEntryRepository);
  new RestController(
    httpServer,
    calculateStock,
    increaseStock,
    decreaseStock,
    cleanStock,
  );
  const queue = new RabbitMQAdapter();
  await queue.connect();
  new QueueController(queue, decreaseStock);
  httpServer.listen(Number(process.env.API_PORT));
}

main();
