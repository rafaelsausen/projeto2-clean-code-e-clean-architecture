import dotenv from 'dotenv';
/* eslint-disable @typescript-eslint/no-explicit-any */
import Hapi from '@hapi/hapi';
import IHttpServer from './repositories/IHttpServer';

dotenv.config();

export default class HapiHttpServer implements IHttpServer {
  server: Hapi.Server;

  constructor() {
    this.server = Hapi.server({
      port: Number(process.env.API_PORT),
      host: process.env.API_HOST,
      routes: {
        cors: true,
      },
    });
  }

  convertUrl(url: string) {
    return url.replace(/\$/g, '');
  }

  async on(method: string, url: string, fn: any): Promise<void> {
    this.server.route({
      method,
      path: this.convertUrl(url),
      handler: async function (request: any, reply: any) {
        try {
          const data = await fn(request.params, request.payload);
          return data;
        } catch (error: any) {
          return reply
            .response({
              message: error.message,
            })
            .code(422);
        }
      },
    });
  }

  async listen(port: number): Promise<void> {
    this.server.settings.port = port;
    await this.server.start();
    // eslint-disable-next-line no-console
    console.log(
      `🏆 Server started with HAPI on port ${`${process.env.API_URL}/`}`,
    );
  }
}
