export default interface IConnection {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  query(statement: string, params: unknown): Promise<any>;
  close(): Promise<void>;
}
