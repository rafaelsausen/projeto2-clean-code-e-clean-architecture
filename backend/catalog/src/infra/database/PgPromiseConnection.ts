/* eslint-disable @typescript-eslint/no-explicit-any */
import dotenv from 'dotenv';
import IConnection from './IConnection';
import pgp from 'pg-promise';

export default class PgPromiseConnection implements IConnection {
  pgp: any;

  constructor() {
    dotenv.config();
    this.pgp = pgp()(`${process.env.DATABASE}`);
  }

  query(statement: string, params: any): Promise<any> {
    return this.pgp.query(statement, params);
  }

  close(): Promise<void> {
    return this.pgp.$pool.end();
  }
}
