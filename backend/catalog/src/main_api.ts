import dotenv from 'dotenv';
import GetProduct from './application/GetProduct';
import GetProducts from './application/GetProducts';
import RestController from './infra/controller/RestController';
import ProductDataDatabase from './infra/repositories/ProductDataDatabase';
import PgPromiseConnection from './infra/database/PgPromiseConnection';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ExpressHttpServer from './infra/http/ExpressHttpServer';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import HapiHttpServer from './infra/http/HapiHttpServer';

dotenv.config();

export default async function main() {
  const connection = new PgPromiseConnection();
  const httpServer =
    Number(process.env.API_PACKAGE) === 1
      ? new HapiHttpServer()
      : new ExpressHttpServer();
  const productData = new ProductDataDatabase(connection);
  const getProduct = new GetProduct(productData);
  const getProducts = new GetProducts(productData);
  new RestController(httpServer, getProducts, getProduct);
  httpServer.listen(Number(process.env.API_PORT));
}

main();
