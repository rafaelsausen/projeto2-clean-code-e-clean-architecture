import GetProduct from '../../src/application/GetProduct';
import IConnection from '../../src/infra/database/IConnection';
import PgPromiseConnection from '../../src/infra/database/PgPromiseConnection';
import ProductDataDatabase from '../../src/infra/repositories/ProductDataDatabase';

test('Deve calcular o volume do produto', async function () {
  const connection: IConnection = new PgPromiseConnection();
  const getProduct = new GetProduct(new ProductDataDatabase(connection));
  const product = await getProduct.execute(1);
  expect(product.idProduct).toBe(1);
  await connection.close();
});
