import GetProducts from '../../src/application/GetProducts';
import PgPromiseConnection from '../../src/infra/database/PgPromiseConnection';
import ProductDataDatabase from '../../src/infra/repositories/ProductDataDatabase';

test('Deve calcular o volume do produto', async function () {
  const connection = new PgPromiseConnection();
  const getProduct = new GetProducts(new ProductDataDatabase(connection));
  const products = await getProduct.execute();
  expect(products[0].idProduct).toBe(1);
  await connection.close();
});
