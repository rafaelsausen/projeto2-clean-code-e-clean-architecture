import axios from 'axios';

axios.defaults.validateStatus = function () {
  return true;
};
import dotenv from 'dotenv';
import supertest from 'supertest';
import request from 'supertest';

import main from '../../src/main_api';

supertest(main);

jest.setTimeout(15000);

dotenv.config();
const url = `${process.env.API_URL}`;

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

beforeAll(async () => {
  await delay(1000);
});

test('Deve retornar a lista de produtos', async function () {
  const response = await request(url).get('/products');
  const output = response.body;
  expect(output).toHaveLength(4);
});

test('Deve retornar um produto', async function () {
  const response = await request(url).get('/products/1');
  const output = response.body;
  expect(output.idProduct).toBe(1);
  expect(output.description).toBe('A');
  expect(output.price).toBe(1000);
  expect(output.volume).toBe(0.03);
  expect(output.density).toBe(100);
});

test('Deve retornar um erro', async function () {
  const response = await request(url).get('/products/1154543215');
  expect(response.status).toBe(422);
  const output = response.body;
  expect(output.idProduct).toBe(undefined);
});
