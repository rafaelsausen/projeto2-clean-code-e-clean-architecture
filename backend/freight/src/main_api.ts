import dotenv from 'dotenv';
import CalculateFreight from './application/CalculateFreight';
import RestController from './infra/controller/RestController';
import ZipcodeDataDatabase from './infra/repositories/ZipcodeDataDatabase';
import PgPromiseConnection from './infra/database/PgPromiseConnection';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ExpressHttpServer from './infra/http/ExpressHttpServer';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import HapiHttpServer from './infra/http/HapiHttpServer';

dotenv.config();

export default async function main() {
  const connection = new PgPromiseConnection();
  const httpServer =
    Number(process.env.API_PACKAGE) === 1
      ? new HapiHttpServer()
      : new ExpressHttpServer();
  const zipcodeData = new ZipcodeDataDatabase(connection);
  const calculateFreight = new CalculateFreight(zipcodeData);
  new RestController(httpServer, calculateFreight);
  httpServer.listen(Number(process.env.API_PORT));
}

main();
