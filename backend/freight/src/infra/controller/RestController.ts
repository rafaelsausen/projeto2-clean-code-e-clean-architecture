import CalculateFreight from '../../application/CalculateFreight';
import IHttpServer from '../http/repositories/IHttpServer';

export default class RestController {
  constructor(
    readonly httpServer: IHttpServer,
    readonly calculateFreight: CalculateFreight,
  ) {
    httpServer.on(
      'post',
      '/calculateFreight',
      async function (params: any, body: any) {
        const output = await calculateFreight.execute(body);
        return output;
      },
    );
  }
}
