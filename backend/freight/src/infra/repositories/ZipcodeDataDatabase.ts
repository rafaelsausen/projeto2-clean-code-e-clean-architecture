import ZipcodeData from '../../domain/repositories/ZipcodeData';
import Zipcode from '../../domain/entities/Zipcode';
import IConnection from '../database/IConnection';

export default class ZipcodeDataDatabase implements ZipcodeData {
  constructor(readonly connection: IConnection) {}

  async get(code: string): Promise<Zipcode | undefined> {
    const [zipcodeData] = await this.connection.query(
      'select * from cccat9.zipcode where code = $1',
      [code],
    );
    if (!zipcodeData) return;
    return new Zipcode(
      zipcodeData.code,
      zipcodeData.street,
      zipcodeData.neighborhood,
      parseFloat(zipcodeData.lat),
      parseFloat(zipcodeData.long),
    );
  }
}
