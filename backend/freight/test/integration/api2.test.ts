import axios from 'axios';

axios.defaults.validateStatus = function () {
  return true;
};
import dotenv from 'dotenv';
import supertest from 'supertest';
import request from 'supertest';

import main from '../../src/main_api';

supertest(main);

jest.setTimeout(15000);

dotenv.config();
const url = `${process.env.API_URL}`;

function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

beforeAll(async () => {
  await delay(1000);
});

test('Deve calcular o frete', async function () {
  const input = {
    from: '22030060',
    to: '88015600',
    items: [{ volume: 0.03, density: 100, quantity: 1 }],
  };
  const response = await request(url).post('/calculateFreight').send(input);
  const output = response.body;
  expect(output.total).toBe(22.45);
});

test('Deve retornar erro', async function () {
  const input = {
    from: '22030060',
    to: '88015600',
    items: [],
  };
  const response = await request(url).post('/calculateFreight').send(input);
  expect(response.status).toBe(422);
});
