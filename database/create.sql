DROP TABLE cccat9.stock_entry;
DROP TABLE cccat9.zipcode;
DROP TABLE cccat9.item;
DROP TABLE cccat9.product;
DROP TABLE cccat9.coupon;
DROP TABLE cccat9.order;
DROP SCHEMA cccat9;

CREATE SCHEMA cccat9;
CREATE TABLE cccat9.product (
	id_product INTEGER PRIMARY KEY,
	description TEXT,
	price NUMERIC,
    width INTEGER,
	height INTEGER,
	length INTEGER,
	weight NUMERIC,
	currency TEXT
);

INSERT INTO cccat9.product (id_product, description, price, width, height, length, weight, currency) VALUES (1, 'A', 1000, 100, 30, 10, 3, 'BRL');
INSERT INTO cccat9.product (id_product, description, price, width, height, length, weight, currency) VALUES (2, 'B', 5000, 50, 50, 50, 22, 'BRL');
INSERT INTO cccat9.product (id_product, description, price, width, height, length, weight, currency) VALUES (3, 'C', 30, 10, 10, 10, 0.9, 'BRL');
insert into cccat9.product (id_product, description, price, width, height, length, weight, currency) values (4, 'D', 100, 100, 30, 10, 3, 'USD');

CREATE TABLE cccat9.coupon (
	code TEXT PRIMARY KEY,
	percentage NUMERIC,
    expire_date TIMESTAMP
);

INSERT INTO cccat9.coupon (code, percentage, expire_date) VALUES ('VALE20', 20, '2024-02-01T00:00:00');
INSERT INTO cccat9.coupon (code, percentage, expire_date) VALUES ('VALE20_EXPIRED', 20, '2023-02-01T00:00:00');

CREATE TABLE cccat9.order (
	id_order SERIAL PRIMARY KEY,
	coupon_code TEXT,
	coupon_percentage NUMERIC,
	code TEXT,
	cpf TEXT,
	email TEXT,
	issue_date TIMESTAMP,
	freight NUMERIC,
	total NUMERIC,
	sequence INTEGER
);

CREATE TABLE cccat9.item (
	id_order INTEGER REFERENCES cccat9.order (id_order),
	id_product INTEGER REFERENCES cccat9.product (id_product),
	price NUMERIC,
	quantity INTEGER,
	PRIMARY KEY (id_order, id_product)
);

CREATE TABLE cccat9.zipcode (
	code TEXT PRIMARY KEY,
	street TEXT,
	neighborhood TEXT,
	lat NUMERIC,
	long NUMERIC
);

INSERT INTO cccat9.zipcode (code, street, neighborhood, lat, long) VALUES ('22030060', '', '', -27.5945, -48.5477);
INSERT INTO cccat9.zipcode (code, street, neighborhood, lat, long) VALUES ('88015600', '', '', -22.9129, -43.2003);


CREATE TABLE cccat9.stock_entry (
	id_stock_entry SERIAL PRIMARY KEY,
	id_product INTEGER REFERENCES cccat9.product (id_product),
	operation TEXT,
	quantity INTEGER
);